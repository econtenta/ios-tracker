# EcontentaLib

[![CI Status](https://img.shields.io/travis/Eldar Dzheparov/EcontentaLib.svg?style=flat)](https://travis-ci.org/Eldar Dzheparov/EcontentaLib)
[![Version](https://img.shields.io/cocoapods/v/EcontentaLib.svg?style=flat)](https://cocoapods.org/pods/EcontentaLib)
[![License](https://img.shields.io/cocoapods/l/EcontentaLib.svg?style=flat)](https://cocoapods.org/pods/EcontentaLib)
[![Platform](https://img.shields.io/cocoapods/p/EcontentaLib.svg?style=flat)](https://cocoapods.org/pods/EcontentaLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EcontentaLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EcontentaLib'
```

