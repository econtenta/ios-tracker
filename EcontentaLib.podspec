#
# Be sure to run `pod lib lint EcontentaLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'EcontentaLib'
  s.version          = '1.1.0'
  s.summary          = 'E-Contenta Tracking Library'

  s.homepage         = 'https://e-contenta.com/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Copyright', :file => 'LICENSE' }
  s.author           = { 'Solution Architects' => 'info@solutionarchitects.tech' }
  s.source           = { :git => 'https://bitbucket.org/econtenta/ios-tracker.git',
    :tag => s.version.to_s }

  s.source_files = 'EcontentaLib/Classes/**/*'
  

  s.swift_version = '5.0'
  s.platform = :ios, "11.0"
  
  # s.resource_bundles = {
  #   'EcontentaLibrary' => ['EcontentaLibrary/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Moya/RxSwift'
  s.dependency 'RxCocoa'
end

