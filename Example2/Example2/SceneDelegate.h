//
//  SceneDelegate.h
//  Example2
//
//  Created by Eldar Dzheparov on 31.12.2021.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

