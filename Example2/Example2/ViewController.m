#import "ViewController.h"
#import "EcontentaLib-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ECOptions* options = [[ECOptions alloc] init];
    options.applicationId = @"appId";
    options.partnerId = @"partnerId";
    options.uid = @"userId";
    
    [ECTracker sharedTracker].options = options;
    
    ECAddToCart* event = [[ECAddToCart alloc] init];
    event.page = @"page";
    [event addWithId:1 name:@"name" price:0.99 currency:@"RUB" category:@"categoty" subcategory:@"subcategory"];
    

    [[ECTracker sharedTracker] eventWithEvent:event];

}


@end
