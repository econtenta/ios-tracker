import Foundation
import RxCocoa
import RxSwift
import Moya

protocol MainRepo {
    func event(request: ECRequestDTO) -> Single<Response>
}

class DefaultMainRepo: MainRepo {
    
    let provider : ApiProvider<MainAPI>

    init(provider:ApiProvider<MainAPI>){
        self.provider = provider
    }
    
    func event(request: ECRequestDTO) -> Single<Response> {
        return provider.request(.event(request))
    }

}

