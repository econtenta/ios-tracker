import Foundation

public enum Events: String {
    case no_events, add_to_cart, purchase, start_view, stop_view, viewing,
         ad_imp, ad_click, click, search, scroll
}

@objc public protocol ECEvent{
    func execute(options:ECOptions)
}

@objc
public class ECAddToCart: NSObject, Encodable, ECEvent{
    
    @objc public var page: String?
    
    private var items:[ECAddToCartItem] = []
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.add_to_cart.rawValue.uppercased(), add_to_cart: items, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    @objc public func add(id: Int, name: String, price: Double, currency: String, category: String?, subcategory: String?){
        
        self.items.append(ECAddToCartItem(id: id, name: name, price: price, currency: currency, category: category, subcategory: subcategory))
    }
}

struct ECAddToCartItem: Encodable{
    var id: Int
    var name: String
    var price: Double
    var currency: String
    var category: String?
    var subcategory: String?
}

@objc
public class ECPurchase: NSObject, Encodable, ECEvent{
    
    @objc public var page: String?
    
    private var items:[ECPurchaseItem] = []
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu:page, et: Events.purchase.rawValue.uppercased(), add_to_cart: nil, purchase: items, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    @objc public func add(id: Int, name: String, price: Double, currency: String, category: String?, subcategory: String?){
        
        self.items.append(ECPurchaseItem(id: id, name: name, price: price, currency: currency, category: category, subcategory: subcategory))
    }
}

struct ECPurchaseItem: Encodable{
    var id: Int
    var name: String
    var price: Double
    var currency: String
    var category: String?
    var subcategory: String?
}


@objc
public class ECStartView: NSObject, Encodable, ECEvent{
    @objc public var id:Int = 0
    @objc public var name: String?
    @objc public var category: String?
    @objc public var subcategory: String?
    
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.start_view.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: self, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case category, subcategory, id, name
    }
    
}

@objc
public class ECStopView: NSObject, Encodable, ECEvent{
    @objc public var id:Int = 0
    @objc public var name: String?
    @objc public var category: String?
    @objc public var subcategory: String?
    @objc public var value: Double = 0.00
    
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.stop_view.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: self, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case value, category, subcategory, id, name
    }
}


@objc
public class ECViewing: NSObject, Encodable, ECEvent{
    @objc public var id:Int = 0
    @objc public var name: String?
    @objc public var category: String?
    @objc public var subcategory: String?
    @objc public var value: Double = 0.00
    
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: "page", et: Events.viewing.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: self, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case value, category, subcategory, id, name
    }
}

@objc
public class ECSearch: NSObject, Encodable, ECEvent{
    @objc public var value: String?
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: self.page, et: Events.search.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: self, ad_imp: nil, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case value
    }
}

@objc
public class ECAdImp: NSObject, Encodable, ECEvent{
    @objc public var placementId: String?
    @objc public var width: Int = 0
    @objc public var height: Int = 0
    @objc public var href: String?
    @objc public var category: String?
    @objc public var subcategory: String?
    
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.ad_imp.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: self, ad_click: nil, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case placementId = "placement_id", width, height, href, category, subcategory
    }
}

@objc
public class ECAdClick: NSObject, Encodable, ECEvent{
    @objc public var placementId: String?
    @objc public var width: Int = 0
    @objc public var height: Int = 0
    @objc public var href: String?
    @objc public var category: String?
    @objc public var subcategory: String?
    
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.ad_click.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: self, scroll: nil, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case placementId = "placement_id", width, height, href, category, subcategory
    }
}

@objc
public class ECScroll: NSObject, Encodable, ECEvent{
    @objc public var value: Double = 0.00
    @objc public var category: String?
    @objc public var subcategory: String?
    
    @objc public var page: String?
    
    @objc public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: page, et: Events.scroll.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: self, click: nil)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case value, category, subcategory
    }
}

@objc
public class ECClick: NSObject, Encodable, ECEvent{
    @objc public var value: String?
    @objc public var page: String?
    
    public override init(){}
    
    public func execute(options: ECOptions) {
        let request = ECRequestDTO(bid: options.bundle ?? "", pid: options.partnerId ?? "", uid: options.uid ?? "", v: env.version, t: Date().currentTimeMillis(), pu: self.page, et: Events.click.rawValue.uppercased(), add_to_cart: nil, purchase: nil, start_view: nil, stop_view: nil, viewing: nil, search: nil, ad_imp: nil, ad_click: nil, scroll: nil, click: self)
        
        ApiClient.shared.call(requestDTO: request)
    }
    
    private enum CodingKeys: String, CodingKey {
        case value
    }
}
