import Foundation

@objc
public class ECOptions: NSObject {
    
    @objc public var bundle: String?
    @objc public var partnerId: String?
    @objc public var uid: String?
    
    public override init(){}
}


@objc
public class ECTracker: NSObject{
    @objc public static let sharedTracker = ECTracker()

    @objc public var options: ECOptions?

    private override init() {}
    
    @objc public static func initialize(options: ECOptions){
        sharedTracker.options = options
    }
    
    @objc public func event(event:ECEvent){
        guard let opts = self.options else{
            return
        }
        
        event.execute(options: opts)
    }
}

enum Enviroment {
    case prod
    case dev
}

let env: Enviroment = {
  #if DEBUG
  return Enviroment.dev
  #else
  return Enviroment.prod
  #endif
}()


extension Enviroment {
  var isDebug: Bool {
    #if DEBUG
    return true
    #else
    return false
    #endif
  }
    var baseURL: URL {
        switch self {
            case .dev: return URL(string: "http://mlog.rktch.com/")!
            case .prod: return URL(string: "http://mlog.rktch.com/")!
        }
    }
    
    var version: String {
        return "1.0.9"
    }
}

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

