import Foundation
import Moya
import RxSwift

public class ApiClient {
    static let shared = ApiClient()
    private init() {}

    func call(requestDTO: ECRequestDTO){
//        let logger = NetworkLoggerPlugin(verbose:true)
//        let provider = ApiProvider<MainAPI>(plugins:[logger])
        
        let provider = ApiProvider<MainAPI>()
        let repo = DefaultMainRepo(provider: provider)
        
        repo.event(request: requestDTO).subscribe()
    }
}

