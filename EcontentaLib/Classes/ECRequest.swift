import Foundation

struct ECRequestDTO: Encodable {
    var bid: String
    var pid: String
    var uid: String
    var v: String
    var t: Int64
    var pu: String? = nil
    var et: String
    var add_to_cart: [ECAddToCartItem]? = nil
    var purchase: [ECPurchaseItem]? = nil
    var start_view: ECStartView? = nil
    var stop_view: ECStopView? = nil
    var viewing: ECViewing? = nil
    var search: ECSearch? = nil
    var ad_imp: ECAdImp? = nil
    var ad_click: ECAdClick? = nil
    var scroll: ECScroll? = nil
    var click: ECClick? = nil
}

