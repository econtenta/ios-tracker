import Foundation
import Moya

enum MainAPI: TargetType{
    case event(ECRequestDTO)
}

extension MainAPI{
    var baseURL: URL { env.baseURL }
    
    var path: String{
        switch self {
            case .event: return "event"
        }
    }
    
    var method: Moya.Method {
        switch self {
            case .event: return .post
        }
    }
    
    var sampleData: Data { Data() }
    
    var task: Task {
      switch self {
      case let .event(requestDTO):
        return .requestJSONEncodable(requestDTO)
      }
        
    }
    
    var headers: [String: String]? {
      switch self {
      case .event:
            return ["Accept": "application/json",
                    "Content-Type": "application/json; charset=utf-8"]
        }
    }
}

