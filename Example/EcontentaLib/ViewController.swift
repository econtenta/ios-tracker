import UIKit
import EcontentaLib

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var options = ECOptions()
        options.bundle = "appId"
        options.partnerId = "partnerId"
        options.uid = "userId"
        
        ECTracker.initialize(options: options)
        
        let tracker = ECTracker.sharedTracker
        
        var click  = ECClick()
        click.page = "page click"
        click.value = "value click"
        
        tracker.event(event: click)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

